# Changelog

#### v0.1.1

- Add CLI option for `--help` to show the help.
- Add CLI option for `--version` to show the version.
- Add CLI option for `--only-defs` to ignore implementation modules.
- Add CLI option for `--log-unused` to trace unused modules.
- Add CLI option for `--prune-sinks` to allow pruning of sinks (i.e. remove modules that are not found in the search directories).

### v0.1

- Initial commit
