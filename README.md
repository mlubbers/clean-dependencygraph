# dependencygraph

This repository contains the dependencygraph tool for visualising dependencies of Clean modules.

## Usage

```
Usage: ./bin/dependencygraph [OPTS] MODULE DIR [DIR [DIR ..]]
Generate a dependency graph for a Clean module.

Options:
  -h  --help            Show this help.
      --version         Show the version.
  -d  --only-defs       Enable or disable only reading definition modules (main modules also).
      --no-only-defs
  -l  --log-unused      Enable or disable the logging of unused modules (i.e. modules in the tree but not reachable from MODULE) on stderr.
      --no-log-unused
  -p  --prune-sinks     Enable or disable pruning the sinks of graph (i.e. remove modules that are not found in the search directories).
      --no-prune-sinks
```

E.g. to find all `.dcl` dependencies of the tool of `clean-platform` itself
run:

```
./bin/dependencygraph --only-defs --prune-sinks dependencygraph src nitrile-packages/linux-x64/clean-platform/lib | dot -Tpng > doc/example.png
```

Which renders as:

![The dependency graph of the dependencygraph tool itself](doc/example.png)

## Maintainer

Mart Lubbers (clean@martlubbers.net)

## License

`dependencygraph` is licensed under the BSD 2-Clause "Simplified" License (see [LICENSE](LICENSE)).
