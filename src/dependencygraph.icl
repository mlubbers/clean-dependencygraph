module dependencygraph

import Clean.ModuleFinder
import Clean.Parse
import Data.Error
import Data.Func
import Data.Functor
from Data.Map import :: Map, instance Functor (Map k)
import qualified Data.Map
from Data.Set import :: Set, class Foldable, instance Foldable Set
import qualified Data.Set
import System.File
from Text import class Text(endsWith,join,replaceSubString), instance Text String, concat5
import System.CommandLine
import System.FilePath
import System.GetOpt

import syntax

:: CLI =
	{ help       :: Bool
	, version    :: Bool
	, logUnused  :: Bool
	, pruneSinks :: Bool
	, onlyDefs   :: Bool
	, mfo        :: ModuleFindingOptions
	}

defaultMFO = {ModuleFindingOptions|include_paths=[],include_libraries=[],clean_home="",include_applications=True}

Start w
	# ([argv0:args], w) = getCommandLine w
	# (opts, nopts, err) = getOpt Permute optdescrs args
	| err =: [_:_] = usage argv0 (die err w)
	# opts = foldr ($) {help=False,version=False,logUnused=False,pruneSinks=False,onlyDefs=False,mfo=defaultMFO} opts
	| opts.help = usage argv0 w
	| opts.version = logerr ["version 0.1.1\n"] w
	= case nopts of
		[] = usage argv0 (die ["Please provide a main module and some search directories\n"] w)
		[_] = usage argv0 (die ["Please provide at least one search directory\n"] w)
		[main:dirs=:[_:_]]
			# opts & mfo = {opts.mfo & include_paths=dirs}
			# (graph, w) = scanner opts 'Data.Set'.newSet 'Data.Map'.newMap [main] w
			# graph = if opts.pruneSinks (pruneSinks graph) graph
			# (_,  fps, w) = findAllModules opts.mfo w
			# w = case if opts.logUnused (findExtra fps ('Data.Map'.keys graph)) [] of
				[] = w
				ms = logerr ["Unused modules:\n":map (\x->x +++ "\n") ms] w
			# (io, w) = stdio w
			# io = foldl (flip fwrites) io (todot graph)
			= snd (fclose io w)
where
	usage :: !String !*World -> *World
	usage argv0 w = logerr [usageInfo (concat5
		"Usage: " argv0 " [OPTS] MODULE DIR [DIR [DIR ..]]\n"
		"Generate a dependency graph in dot format for a Clean module.\n\n"
		"Options:") optdescrs, "\n"] w

	optdescrs :: [OptDescr (CLI -> CLI)]
	optdescrs =
		[ Option ['h'] ["help"]       (NoArg \o->{o & help=True})
			"Show this help."
		, Option []    ["version"]    (NoArg \o->{o & version=True})
			"Show the version."
		, Option ['d'] ["only-defs"]  (NoArg \o->{o & onlyDefs=True})
			"Enable or disable only reading definition modules (main modules also)."
		, Option [] ["no-only-defs"]  (NoArg \o->{o & onlyDefs=False}) ""
		, Option ['l'] ["log-unused"] (NoArg \o->{o & logUnused=True})
			"Enable or disable the logging of unused modules (i.e. modules in the tree but not reachable from MODULE) on stderr."
		, Option [] ["no-log-unused"] (NoArg \o->{o & logUnused=False}) ""
		, Option ['p'] ["prune-sinks"]      (NoArg \o->{o & pruneSinks=True})
			"Enable or disable pruning the sinks of graph (i.e. remove modules that are not found in the search directories)."
		, Option [] ["no-prune-sinks"]      (NoArg \o->{o & pruneSinks=False}) ""
		]

pruneSinks :: !(Map String [String]) -> Map String [String]
pruneSinks m = foldr 'Data.Map'.del m ('Data.Set'.toList toremove) <$&> filter (flip 'Data.Set'.notMember toremove)
where
	toremove = 'Data.Set'.fromList [k \\ (k, []) <- 'Data.Map'.toList m]

findExtra :: ![FilePath] ![String] -> [String]
findExtra fp ms = filter (not o used) (map dropExtension fp)
where
	used x = any (\mod->endsWith mod x) mset
	mset = [replaceSubString "." "/" m \\ m <- ms]

todot :: !(Map String [String]) -> [String]
todot mp =
	[ "digraph D { \n"
	: flip (foldr makeNode) ('Data.Map'.keys mp)
	( flip (foldr makeEdge) (flatten [[(k, v)\\v<-vs] \\ (k, vs)<-'Data.Map'.toList mp])
	["}\n"])]
where
	makeNode s acc = ["\t", safe s, " [label=\"", s, "\"];\n":acc]
	makeEdge (fro, to) acc = ["\t", safe fro, " -> ", safe to, ";\n":acc]
	safe s = replaceSubString "." "" s

die :: ![String] !*World -> *World
die msg w = setReturnCode 1 (logerr msg w)

logerr :: ![String] !*World -> *World
logerr msgs w = snd (fclose (foldl (flip fwrites) stderr msgs) w)

scanner :: !CLI !(Set String) !(Map String [String]) ![String] !*World -> (!Map String [String], !*World)
scanner _ _ graph [] w = (graph, w)
scanner opts vis graph [mod:mods] w
	| 'Data.Set'.member mod vis = scanner opts vis graph mods w
	# vis = 'Data.Set'.insert mod vis
	# (paths, w) = findModule mod opts.mfo w
	# (imports, w) = case paths of
		[] = ([], logerr ["Not in any search path: ", mod, "\n"] w)
		[fp:_]
			# (fps, w) = dclIfExists opts.onlyDefs fp w
			= findImportsInFiles fps [] w
	# graph = 'Data.Map'.put mod imports graph
	= scanner opts vis graph (mods ++ imports) w
where
	findImportsInFiles :: [String] [String] !*World -> (![String], !*World)
	findImportsInFiles [] acc w = (acc, w)
	findImportsInFiles [f:fs] acc w = uncurry (findImportsInFiles fs) $
		case readModule f w of
			(Error e, w) = ([], logerr ["Error reading ", f, ": ", e] w)
			(Ok (pm, _), w) = (foldr findImports acc pm.mod_defs, w)
	where
		findImports :: !ParsedDefinition ![String] -> [String]
		findImports (PD_Import pis) acc = [p.import_module.id_name\\p<-pis] ++ acc
		findImports _ acc = acc

	dclIfExists :: !Bool !String !*World -> (![String], !*World)
	dclIfExists onlydefs icl w
		# dcl = dropExtension icl <.> "dcl"
		# (exists, w) = fileExists dcl w
		| onlydefs = ([if exists dcl icl], w)
		= ([icl:if exists [dcl] []], w)
